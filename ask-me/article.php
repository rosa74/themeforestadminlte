<?php
include 'config.php';

$id = isset($_GET["id"]) ? $_GET["id"] : "";

    $req = $pdo->prepare("SELECT * FROM article WHERE id = :id");
    $req->execute(['id'=>$id]);
    //$result = $req->fetchALL();
    
    $infos = $req->fetch();

$req = $pdo->prepare("SELECT * FROM commentaire WHERE id = ?");
$req->execute([$id]);
$nb = $req -> rowcount();

   $nom    = isset($_POST['nom'])  && !empty($_POST['nom'])  ? $_POST['nom']  : '';
   $commentaire = isset($_POST['commentaire']) && !empty($_POST['commentaire']) ? $_POST['commentaire']: '';
    

   $submit= isset($_POST['submit'])&& !empty($_POST['submit'])? $_POST['submit'] : '';

   if ((isset($_POST['submit']))) {
       try {
           $request = $pdo->prepare("INSERT INTO commentaire ( id, nom, commentaire, date_creation) VALUES (:id, :nom, :commentaire, :date_creation)");
   
           $request->execute([
               'id'=>$id,
               'nom'=>$nom,
               'commentaire'=>$commentaire,
               'date_creation'=>date('Y-m-d'),
               ]);
               header("Location: article.php?id=$id");

       }
       catch (PDOException $e) {
           echo 'Error: '.$e->getMessage();
       }
     
   }           
include 'header.php';

?>
 
 <section class="container main-content">
    <div class="row">
        <div class="col-md-12">
            <article class="post single-post clearfix">
                <div class="post-inner">
                    <div class="post-img"><img src="<?php echo $infos['photo']?>" alt="..."></div>
                    <h2 class="post-title"><span class="post-type"><i class="icon-film"></i></span><?php echo $infos['titre']?></h2>
                        <div class="post-meta">
                            <span class="meta-date"><i class="icon-time"></i><?php echo $infos['date_creation']?></span>
                            <span class="meta-comment"><i class="icon-comments-alt"></i><a href="#"><?php echo $nb ?></a></span>
                        </div>
                    <div class="post-content">
                        <p><?php echo $infos['texte']?></p>
                    </div><!-- End post-content -->
                    <div class="clearfix"></div>
                </div><!-- End post-inner -->
            </article><!-- End article.post -->
        </div>
    </div>

    
 
    <div id="commentlist" class="page-content">
        <div class="boxedtitle page-title"><h2>Comments ( <span class="color"><?php echo $nb ?></span> )</h2></div>
        <?php
        $req = $pdo->prepare("SELECT * FROM commentaire WHERE id = ? ORDER BY date_creation DESC");
        $req ->execute([$id]);
        $result = $req ->fetchAll();
        foreach ($result as $value) { ?>
        <ol class="commentlist clearfix">
            <li class="comment">
                <div class="comment-body clearfix"> 
                    <div class="comment-text">
                        <div class="author clearfix">
                            <div class="comment-meta">
                                <span><?php echo $value['nom'] ?></span>
                                <div class="date"><?php echo $value['date_creation'] ?></div> 
                            </div>
                        </div>
                        <div class="text"><p><?php echo $value['commentaire'] ?></p>
                        </div>
                    </div>
                </div>
            </li>
        </ol><!-- End commentlist -->
        <?php } ?>
    </div><!-- End page-content -->

    <div id="respond" class="comment-respond page-content clearfix">
                    <div class="boxedtitle page-title"><h2>Laisser un commentaire</h2></div>
                    <form action="" method="post" id="commentform" class="comment-form">
                        <div id="respond-inputs" class="clearfix">
                            <p>
                                <label class="required" for="comment_name">Nom<span>*</span></label>
                                <input name="nom" type="text" value="" id="comment_name" aria-required="true">
                            </p>
                        </div>
                        <div id="respond-textarea">
                            <p>
                                <label class="required" for="comment">Commentaire<span>*</span></label>
                                <textarea id="comment" name="commentaire" aria-required="true" cols="58" rows="10"></textarea>
                            </p>
                        </div>
                        <p class="form-submit">
                            <input name="submit" type="submit" id="submit" value="Post Commentaire" class="button small color">
                        </p>
                    </form>
                </div>
</section>
    
<?php
include 'footer.php';
?>

