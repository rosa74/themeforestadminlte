<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<head>

	<!-- Basic Page Needs -->
	<meta charset="utf-8">
	<title>MON PETIT BLOG</title>
	<meta name="description" content="Ask me Responsive Questions and Answers Template">
	<meta name="author" content="vbegy">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Main Style -->
	<link rel="stylesheet" href="style.css">

	<!-- Skins -->
	<link rel="stylesheet" href="css/skins/skins.css">

	<!-- Responsive Style -->
	<link rel="stylesheet" href="css/responsive.css">

	<!-- Favicons -->
	<link rel="shortcut icon" href="images/favicon.png">

</head>

<body>



	<!-- End header-top -->
	<header id="header">
	
			<img src="images/sun.jpeg" alt="logo" class="brand-image img-circle elevation-3" style="opacity: .8">
			<nav class="navigation">
				<ul>

					<li class="current_page_item parent-list"><a href="pageAccueil.php">Accueil<span class="menu-nav-arrow"></span></a>

					</li>
					<li class="current_page_item "><a href="admin.php">Articles<span class="menu-nav-arrow"></span></a>
				</ul>
				</li>


				</ul>
			</nav>

		</section><!-- End container -->
	</header><!-- End header -->