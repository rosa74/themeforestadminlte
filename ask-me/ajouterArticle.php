<?php


include 'config.php';



?>

<?php

$titre = isset($_POST['titre']) && !empty($_POST['titre']) ? $_POST['titre'] : '';
$description = isset($_POST['texte']) && !empty($_POST['texte']) ? $_POST['texte'] : '';
$extrait = isset($_POST['extrait']) && !empty($_POST['extrait']) ? $_POST['extrait'] : '';
$photo = isset($_FILES['photo']) ? "images/" . microtime() . $_FILES['photo']['name'] : '';

$submit = isset($_POST['submit']) && !empty($_POST['submit']) ? $_POST['submit'] : '';

if ((isset($_POST['submit']))) {
    try {
        $request = $pdo->prepare("INSERT INTO article( titre, `texte`, extrait, date_creation, photo) VALUES (:titre, :texte, :extrait, :date_creation, :photo)");

        $request->execute([
            'titre' => $titre,
            'texte' => $description,
            'extrait' => $extrait,
            'date_creation' => date('Y-m-d'),
            'photo' => $photo,
        ]);
        move_uploaded_file($_FILES["photo"]["tmp_name"], $photo);
    } catch (PDOException $e) {
        echo 'Error: ' . $e->getMessage();
    }
}
?>

<?php
include 'nav.php';
?>

<div class="col-md-6">
    <div class="page-content">
        <h2 style="margin-left: 400px;text-align:center;">Ajouter un article</h2>
        <form action="/THEMEFOREST/ask-me/ajouterArticle.php" class="form-style form-style-3 form-style-5" enctype="multipart/form-data" method="post">
            <div class="form-inputs clearfix">
                <p>
                    <label style="margin-left: 400px;text-align:center;" for="name" class="required">Titre</label>
                    <input style="margin-left: 300px;padding:20px" type="text" class="required-item" value="" name="titre" id="name" aria-required="true">
                </p>
                <p>
                    <label style="margin-left: 400px;text-align:center;" for="mail" class="required">Extrait</label>
                    <input style="margin-left: 300px;padding:20px" type="text" class="required-item" id="mail" name="extrait" value="" aria-required="true">
                </p>
            </div>
            <div class="form-textarea">
                <p>
                    <label style="margin-left: 400px;text-align:center;" for="mail" class="required">Description<span>*</span></label>
                    <textarea style="margin-left: 300px;padding:20px" class="required-item" id="mail" name="texte" value="" aria-required="true">
						</textarea>
                </p>
            </div>
            <p>
                <label style="margin-left: 400px;text-align:center;" for="photo" class="required">Photo<span>*</span></label>
                <input style="margin-left: 300px;padding:20px;text-align:center;" type="file" class="required-item" id="mail" name="photo" value="" aria-required="true">
            </p>

            <p class="form-submit">
                <input style="margin-left: 300px;padding:20px;background-color:red;" name="submit" type="submit" value="Ajouter" class="submit button small color ">
            </p>
        </form>
    </div><!-- End page-content -->
</div>
<?php
include 'foot.php';
?>